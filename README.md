## 文档

1. [项目结构和一些不得不说的话](doc%2F1.md)
2. [从最简单的 TM 开始](doc%2F2.md)
3. [引用计数缓存框架和共享内存数组](doc%2F3.md)
4. [数据页的缓存与管理](doc%2F4.md)
5. [日志文件与恢复策略](doc%2F5.md)
6. [页面索引与 DM 层的实现](doc%2F6.md)
7. [记录的版本与事务隔离](doc%2F7.md)
8. [死锁检测与 VM 的实现](doc%2F8.md)
9. [索引管理器](doc%2F9.md)
10. [字段与表管理](doc%2F10.md)
11. [服务端客户端的实现及其通信规则](doc%2F11.md)


# MYDB

MYDB 是一个 Java 实现的简单的数据库，部分原理参照自 MySQL、PostgreSQL 和 SQLite。实现了以下功能：

- 数据的可靠性和数据恢复
- 两段锁协议（2PL）实现可串行化调度
- MVCC
- 两种事务隔离级别（读提交和可重复读）
- 死锁处理
- 简单的表和字段管理
- 简陋的 SQL 解析（因为懒得写词法分析和自动机，就弄得比较简陋）
- 基于 socket 的 server 和 client

## 运行方式

注意首先需要在 pom.xml 中调整编译版本，如果导入 IDE，请更改项目的编译版本以适应你的 JDK

首先执行以下命令编译源码：

```shell
mvn compile
```

接着执行以下命令以 /tmp/mydb 作为路径创建数据库：

```shell
mvn exec:java -D"exec.mainClass"="top.guoziyang.mydb.backend.Launcher" -D"exec.args"="-create /tmp/mydb"
```

随后通过以下命令以默认参数启动数据库服务：

```shell
mvn exec:java -D"exec.mainClass"="top.guoziyang.mydb.backend.Launcher" -D"exec.args"="-open /tmp/mydb"
```

这时数据库服务就已经启动在本机的 9999 端口。重新启动一个终端，执行以下命令启动客户端连接数据库：

```shell
mvn exec:java -D"exec.mainClass"="top.guoziyang.mydb.client.Launcher"
```

会启动一个交互式命令行，就可以在这里输入类 SQL 语法，回车会发送语句到服务，并输出执行的结果。

一个执行示例：
![demo.png](img%2Fdemo.png)